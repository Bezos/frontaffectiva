var express = require("express");
var app = express();
var server = require("http").Server(app);
var bodyParser = require("body-parser"); 
var config = require("./config.json");
//require('./app/controller.js');

var MongoClient = require("mongodb").MongoClient;
const fs = require('fs');
require('es6-promise').promise;

/*new Promise(function (resolve, reject) {
    MongoClient.connect(
        config.mongoHost,
        function(err, db) {
          if (err){
            resolve("Error getting config" + err)
            console.log("Error on mongo connection: " + err)
          }
          else{
            var dbo = db.db(config.mongoDB);
            dbo
              .collection("dataConfig")
              .findOne({}, function(err, result) {
                if (err) {
                  resolve("Error getting config" + err)               
                  console.log("Error updating config: " + err);
                }
                else if (result) {
                  resolve("Success getting config")  
                  var nuevoValor = { nuevo: 'valorPredeterminado'};     
                  let data = JSON.stringify(result.contentConfig); 
                  //onsole.log('No existe, valor del resultado obtenido de Mongo: ' + data)
                   fs.writeFileSync('./config.json', data);  
                   resolve("Error getting config" + err)         
                  }
                  else  resolve("Error getting config")  
              });
            db.close();
          }
      }
    );
   }).then((Message) => {*/
    
      app.use(bodyParser.urlencoded({ extended: true }));
      app.use(bodyParser.json());

      //Ruta estática de recursos
      app.use('/public', express.static('app/public')); 

      //Rutas 
      var routes = require('./app/routing.js');

      //Dirección raiz
      app.use('/affectiva', routes);

      //Escucha
      server.listen("3030", function () {
        console.log("CORRIENDO msAffectiva EN PUERTO " + "3030" + " !");
      });  
  //});







