var express = require('express');
var router = express.Router();
var fs = require('fs');
var controller = require ('./controller');

router.get("*", function(req, res) {
    res.sendFile(__dirname + "/public/index.html");
});

//router.post("/emotion", controller.getEmotion);

module.exports = router;